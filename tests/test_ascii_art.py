from unittest import TestCase
from pysay.ascii_art import py_format
class TestAsciiArt(TestCase):
    def test_py_format_contains_input_text(self):
        self.assertIn(
            'test-string',
            py_format('test-string')
        )